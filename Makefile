.PHONY: up down ps reload install go-db

up:
	@docker-compose up -d

down:
	@docker-compose down --remove-orphans

reload: down up

install:
	@docker-compose exec api composer install --dev

ps:
	@docker-compose ps

go-db:
	@mysql -h`docker inspect db | jq .[0].NetworkSettings.Networks.seriesapi_default.IPAddress | cut -d \" -f2` -uuser -ppassword series