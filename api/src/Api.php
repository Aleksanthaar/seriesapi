<?php

use App\Services\Connection;
use App\Services\Foo;
use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$container = new Container();

$container->set('messageBoard', function () {
    return new Foo("Bar");
});

$container->set('connection', function () {
    return new Connection(getenv('MYSQL_DATABASE'), getenv('MYSQL_HOST'), getenv('MYSQL_USER'), getenv('MYSQL_PASSWORD'));
});

AppFactory::setContainer($container);

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
    $message = 'Hello World!';

    if ($this->has('messageBoard')) {
        $message = $this->get('messageBoard')->getMessage();
    }

    $response->getBody()->write($message);

    return $response;
});

return $app;