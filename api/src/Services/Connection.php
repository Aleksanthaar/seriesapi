<?php

namespace App\Services;

use PDO;

class Connection
{
    protected PDO $pdo;

    public const DSN = 'mysql:dbname=%s;host=%s'

    public function __construct($db, $host, $user, $password)
    {
        $dsn       = sprtintf(static::DSN, $db, $host);
        $this->pdo = new PDO($dsn, $user, $password);
    }
}